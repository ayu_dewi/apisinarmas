<?php
//------------------------------------------------------------------
//Retrieve JSON File
header("Content-Type: application/json; charset=UTF-8");
$input = trim(file_get_contents("php://input"));
$datas = json_decode($input, false);

//------------------------------------------------------------------
//Set variable
foreach ($datas as $data)
{
	$v_pe_code = $data->pe_code;
	$v_signature = $data->signature;
	$v_api_type = $data->api_type;
	$v_merchant_key = $data->merchant_key;
	$v_registration_number = $data->registration_number;
	$v_id_number = $data->id_number;
}

$v_check = true;
$v_RSLT_ID = "0";
$v_RSLT_MSG = "";

//------------------------------------------------------------------
// Turn off all error reporting & handle exception
error_reporting(0);
set_error_handler("customError", E_ALL);

function customError($errno, $errstr)
{
	if (strlen(trim($errstr))>0) {
		$v_RSLT_ID = "100";
		//$v_RSLT_MSG = "Error: [$errno] $errstr";
		$v_RSLT_MSG = "There is something wrong. Please try again later.";
		
		$response[] = array(
			'registration_number'=>$v_registration_number,
			'id_number'=>$v_id_number,
			'registration_date'=>null,
			'status'=>null,
			'approval_date'=>null,
			'user_id'=>null,
			'activation'=>null,
			'activation_date'=>null,
			'clientid'=>null,
			'result_id'=>$v_RSLT_ID,
			'result_message'=>$v_RSLT_MSG
		);
		
		echo json_encode($response);
		die();
	}
	else {
		return true;
	}
}

//------------------------------------------------------------------
//Set Variable Constant
require_once('../config/validation.php');
require_once('../config/security.php');
$vc_pe_code = $sec_pe_code;
$vc_signature = $sec_signature;
$vc_api_type = "ActivationStatus";

require_once('../config/merchant_key.php');

//------------------------------------------------------------------
//Validation
if ($v_check && $v_pe_code != $vc_pe_code){
	$v_RSLT_ID = "20";
	$v_RSLT_MSG = "Invalid PE Code";
	$v_check = false;
}

if ($v_check && $v_signature != $vc_signature){
	$v_RSLT_ID = "21";
	$v_RSLT_MSG = "Invalid Signature";
	$v_check = false;
}

if ($v_check && $v_api_type != $vc_api_type){
	$v_RSLT_ID = "22";
	$v_RSLT_MSG = "Invalid API Type";
	$v_check = false;
}

if ($v_check && $cnt_merchant_key == 0){
	$v_RSLT_ID = "25";
	$v_RSLT_MSG = "Invalid Merchant Key";
	$v_check = false;
}

if ($v_check && (isValidStr($v_merchant_key)!='OK' || isValidStr($v_registration_number)!='OK')){
	$v_RSLT_ID = "24";
	$v_RSLT_MSG = "Invalid Input Parameter";
	$v_check = false;
}

if ($v_check && (strlen(trim($v_registration_number))>0 && strlen(trim($v_id_number))>0)){
	$v_RSLT_ID = "96";
	$v_RSLT_MSG = "Invalid Data Value : Fill In Either The Registration Number or Identity Number.";
	$v_check = false;
}

//------------------------------------------------------------------
//Execute API process
if ($v_check){
	require_once('../config/dbconn.php');
	mssql_select_db($db_web, $conn);
	
	mssql_query("SET ANSI_NULLS ON", $conn);
	mssql_query("SET ANSI_WARNINGS ON", $conn);
	
	if (strlen(trim($v_registration_number))>0) {
		$v_filter = 'REGNUMBER';
	}
	else if (strlen(trim($v_id_number))>0) {
		$v_filter = 'IDNUMBER';
	}
	else {
		$v_filter = '';
	}

	$query = "exec API_ActivationStatusByFilter '$v_merchant_key', '$v_filter', '$v_registration_number', '$v_id_number';";
	$exec = mssql_query($query, $conn);
	$numrow = mssql_num_rows($exec);
	
	if (!$exec || $numrow == 0) {
		$registration_number = null;
		$id_number = null;
		
		$registration_date = null;
		$status = null;
		$approval_date = null;
		$user_id = null;
		$activation = null;
		$activation_date = null;
		$clientid = null;
	}
	else {
		while ($row = mssql_fetch_assoc($exec)){
			$registration_number = $row['registration_number'];
			$id_number = $row['id_number'];

			$registration_date = $row['registration_date'];
			$status = $row['status'];
			$approval_date = $row['approval_date'];
			$user_id = $row['user_id'];
			$activation = $row['activation'];
			$activation_date = $row['activation_date'];
			$clientid = $row['clientid'];
		}
	}
	
	$response[] = array(
		'registration_number'=>$v_registration_number,
		'id_number'=>$v_id_number,
		'registration_date'=>$registration_date,
		'status'=>$status,
		'approval_date'=>$approval_date,
		'user_id'=>$user_id,
		'activation'=>$activation,
		'activation_date'=>$activation_date,
		'clientid'=>$clientid,
		'result_id'=>$v_RSLT_ID,
		'result_message'=>$v_RSLT_MSG
	);
	
	echo json_encode($response);
	mssql_close($conn);
	
} else {
	$response[] = array(
		'registration_number'=>$v_registration_number,
		'id_number'=>$v_id_number,
		'registration_date'=>null,
		'status'=>null,
		'approval_date'=>null,
		'user_id'=>null,
		'activation'=>null,
		'activation_date'=>null,
		'clientid'=>null,
		'result_id'=>$v_RSLT_ID,
		'result_message'=>$v_RSLT_MSG
	);
	echo json_encode($response);
}
//------------------------------------------------------------------
?>